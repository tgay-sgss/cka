#!/bin/bash
# script that runs 
# https://kubernetes.io/docs/setup/production-environment/container-runtime

cat >> /etc/hosts << EOF
{
  192.168.4.111 controller.localdomain control
  192.168.4.112 worker1.localdomain worker1
  192.168.4.113 worker2.localdomain worker2
  192.168.4.113 worker3.localdomain worker3
}
EOF

systemctl enable docker

if [[ $HOSTNAME = controller.localdomain ]]
then
  firewall-cmd --add-port 6443/tcp --permanent
  firewall-cmd --add-port 2379-2380/tcp --permanent
  firewall-cmd --add-port 10250/tcp --permanent
  firewall-cmd --add-port 10251/tcp --permanent
  firewall-cmd --add-port 10252/tcp --permanent
fi

if echo $HOSTNAME | grep worker
then
  firewall-cmd --add-port 10250/tcp --permanent
  firewall-cmd --add-port 30000-32767/tcp --permanent
fi

systemctl restart firewalld
